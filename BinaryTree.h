/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   BinaryTree.h
 * Author: mcmikecamara
 *
 * Created on May 11, 2016, 8:46 PM
 */

#ifndef BINARYTREE_H
#define BINARYTREE_H

#include <iostream>
#include <cstdlib>

using namespace std;

//Definition of the Node

template <class elemType>
struct nodeType {
    elemType info;
    nodeType<elemType> *lLink;
    nodeType<elemType> *rLink;
};

//Definition of the class

template <class elemType>

class binaryTreeType {
public:

    const binaryTreeType<elemType>& operator=
            (const binaryTreeType<elemType>&);
    //Overload the assignment operator.

    bool isEmpty() const;
    //Function to determine whether the binary tree is empty. 
    //Postcondition: Returns true if the binary tree is empty; 
    // otherwise, returns false.

    void inorderTraversal() const;
    //Function to do an inorder traversal of the binary tree. 
    //Postcondition: Nodes are printed in inorder sequence.

    void preorderTraversal() const;
    //Function to do a preorder traversal of the binary tree. 
    //Postcondition: Nodes are printed in preorder sequence.

    void postorderTraversal() const;
    //Function to do a postorder traversal of the binary tree. 
    //Postcondition: Nodes are printed in postorder sequence.

    int treeHeight() const;
    //Function to determine the height of a binary tree. 
    //Postcondition: Returns the height of the binary tree.

    int treeNodeCount() const;
    //Function to determine the number of nodes in a 
    //binary tree.
    //Postcondition: Returns the number of nodes in the 
    // binary tree.

    int treeLeavesCount() const;
    //Function to determine the number of leaves in a
    //binary tree.
    //Postcondition: Returns the number of
    //               binary tree.

    void destroyTree();
    //Function to destroy the binary tree. 
    //Postcondition: Memory space occupied 
    // is deallocated.
    // root = NULL;

    bool search(const elemType& searchItem) const;
    //Function to determine if searchItem is in the binary 
    //tree.
    //Postcondition: Returns true if searchItem is found in 
    // the binary tree; otherwise, returns
    // false.

    void insert(const elemType& insertItem);
    //Function to insert insertItem in the binary tree. 
    //Postcondition: If there is no node in the binary tree 
    // that has the same info as insertItem, a 
    // node with the info insertItem is created 
    // and inserted in the binary search tree.
    
    int size(nodeType<elemType> *p) const;

    void deleteNode(const elemType& deleteItem);
    //Function to delete deleteItem from the binary tree. 
    //Postcondition: If a node with the same info as
    // deleteItem is found, it is deleted from 
    // the binary tree.
    // If the binary tree is empty or
    // deleteItem is not in the binary tree, 
    // an appropriate message is printed.

    binaryTreeType(const binaryTreeType<elemType>& otherTree);
    //Copy constructor

    binaryTreeType();
    //Default constructor

    ~binaryTreeType();
    //Destructor

protected:
    
    nodeType<elemType> *root;
    
private:
    
    void copyTree(nodeType<elemType>* &copiedTreeRoot,
            nodeType<elemType>* otherTreeRoot);
    //Makes a copy of the binary tree to which 
    //otherTreeRoot points.
    //Postcondition: The pointer copiedTreeRoot points to 
    // the root of the copied binary tree.
    
    void destroy(nodeType<elemType>* &p);
    //Function to destroy the binary tree to which p points. 
    //Postcondition: Memory space occupied by each node, in 
    // the binary tree to which p points, is 
    // deallocated.
    // p = NULL;
    
    
    
    void inorder(nodeType<elemType> *p) const;
    //Function to do an inorder traversal of the binary 
    //tree to which p points.
    //Postcondition: Nodes of the binary tree, to which p
    // points, are printed in inorder sequence.
    
    void preorder(nodeType<elemType> *p) const;
    //Function to do a preorder traversal of the binary 
    //tree to which p points.
    //Postcondition: Nodes of the binary tree, to which p 
    // points, are printed in preorder
    // sequence.
    
    void postorder(nodeType<elemType> *p) const;
    //Function to do a postorder traversal of the binary 
    //tree to which p points.
    //Postcondition: Nodes of the binary tree, to which p 
    // points, are printed in postorder
    // sequence.
    
    int height(nodeType<elemType> *p) const;
    //Function to determine the height of the binary tree 
    //to which p points.
    //Postcondition: Height of the binary tree to which 
    // p points is returned.
    
    int max(int x, int y) const;
    //Function to determine the larger of x and y. 
    //Postcondition: Returns the larger of x and y.
    
    int nodeCount(nodeType<elemType> *p) const;
    //Function to determine the number of nodes in
    //the binary tree to which p points. 
    //Postcondition: The number of nodes in the binary 
    // tree to which p points is returned.
    
    int leavesCount(nodeType<elemType> *p) const; 
    //Function to determine the number of leaves in 
    //the binary tree to which p points. 
    //Postcondition: The number of leaves in the binary 
    // tree to which p points is returned.
    
    void deleteFromTree(nodeType<elemType>* &p);
    //Function to delete the node to which p points is 
    //deleted from the binary search tree. 
    //Postcondition: The node to which p points is deleted 
    // from the binary search tree.

};

template <class elemType>
int binaryTreeType<elemType>::size(nodeType<elemType> *p) const 
{
    if(p == NULL) { //This node doesn't exist. Therefore there are no nodes in this 'subtree'
        return 0;
    } else { //Add the size of the left and right trees, then add 1 (which is the current node)
        return size(p->lLink) + size(p->rLink) + 1;
    }
}


template <class elemType>
bool binaryTreeType<elemType>::isEmpty() const
{
    return (root == NULL); 
}

template <class elemType> binaryTreeType<elemType>::binaryTreeType() 
{
    root = NULL;
}

template <class elemType>
void binaryTreeType<elemType>::inorderTraversal() const 
{
    inorder(root);
}

template <class elemType>
void binaryTreeType<elemType>::preorderTraversal() const 
{
    preorder(root);
}

template <class elemType>
void binaryTreeType<elemType>::postorderTraversal() const 
{
    postorder(root);
}

template <class elemType>
int binaryTreeType<elemType>::treeHeight() const
{
    return height(root); 
}

template <class elemType>
int binaryTreeType<elemType>::treeNodeCount() const
{
    return nodeCount(root); 
}

template <class elemType>
int binaryTreeType<elemType>::treeLeavesCount() const
{
    return leavesCount(root); 
}

template <class elemType>
void binaryTreeType<elemType>::inorder(nodeType<elemType> *p) const
{
    if (p != NULL) 
    {
        inorder(p->lLink);
        cout << p->info << " ";
        inorder(p->rLink);
    }
}

template <class elemType>
void binaryTreeType<elemType>::preorder(nodeType<elemType> *p) const
{
    if (p != NULL) 
    {
        cout << p->info << " ";
        preorder(p->lLink);
        preorder(p->rLink);
    }
}

template <class elemType>
void binaryTreeType<elemType>::postorder(nodeType<elemType> *p) const
{
    if (p != NULL)
    {
        postorder(p->lLink);
        postorder(p->rLink);
        cout << p->info << " ";
    }
}

template<class elemType>
int binaryTreeType<elemType>::height(nodeType<elemType> *p) const
{
    if (p == NULL)
    {
        return 0;
    }
    else
    {
        return 1 + max(height(p->lLink), height(p->rLink));
    }
}

template <class elemType>
int binaryTreeType<elemType>::max(int x, int y) const
{
    if (x >= y)
    {
        return x;
    }
    else
    {
        return y;
    }
}

template <class elemType>
void binaryTreeType<elemType>::copyTree
(nodeType<elemType>* &copiedTreeRoot, nodeType<elemType>* otherTreeRoot)
{
    if (otherTreeRoot == NULL)
    {
        copiedTreeRoot = NULL;   
    }
    else
    {
        copiedTreeRoot = new nodeType<elemType>; 
        copiedTreeRoot->info = otherTreeRoot->info; 
        copyTree(copiedTreeRoot->lLink, otherTreeRoot->lLink); 
        copyTree(copiedTreeRoot->rLink, otherTreeRoot->rLink);
    }
}

template <class elemType>
void binaryTreeType<elemType>::destroy(nodeType<elemType>* &p)
{
    if (p != NULL)
    {
        destroy(p->lLink); destroy(p->rLink); 
        delete p;
        p = NULL;
    }
}

template <class elemType>
void binaryTreeType<elemType>::destroyTree() 
{
    destroy(root);
}

//copy constructor
template <class elemType> binaryTreeType<elemType>::binaryTreeType (const binaryTreeType<elemType>& otherTree)
{
    if (otherTree.root == NULL) 
    {//otherTree is empty 
        root = NULL;
    }
    else
    {
        copyTree(root, otherTree.root);
    }
}

//Destructor
template <class elemType> binaryTreeType<elemType>:: ~binaryTreeType() {
    destroy(root);
}

//Overload the assignment operator
template <class elemType>
const binaryTreeType<elemType>& binaryTreeType<elemType>::operator=(const binaryTreeType<elemType>& otherTree)
{
    if (this != &otherTree) //avoid self-copy
    {
        if (root != NULL) //if the binary tree is not empty, //destroy the binary tree
        {
            destroy(root);
        }
   
        if (otherTree.root == NULL) //otherTree is empty 
        {
            root = NULL;
        }
        else
        {
            copyTree(root, otherTree.root);
        } //end else
        
        return *this;
        
    } //end if
}// end

template <class elemType>
bool binaryTreeType<elemType>::search(const elemType& searchItem) const
{
    nodeType<elemType> *current;
    bool found = false;
    
    if (root == NULL)
    {
        cout << "Cannot search an empty tree." << endl;
    }
    else
    {
        current = root;
        
        while (current != NULL && !found)
        {
            if (current->info == searchItem)
            { 
                found = true;
            }
            else if (current->info > searchItem) 
            {
                current = current->lLink;
            }
            else
            {
                current = current->rLink;
            }
        }//end while
    }//end else
    
    return found;
}// end search


template <class elemType>
void binaryTreeType<elemType>::insert(const elemType& insertItem)
{
    nodeType<elemType> *current; //pointer to traverse the tree
    nodeType<elemType> *trailCurrent; //pointer behind current 
    nodeType<elemType> *newNode; //pointer to create the node
    
    newNode = new nodeType<elemType>; 
    newNode->info = insertItem; newNode->lLink = NULL; 
    newNode->rLink = NULL;
    
    if (root == NULL)
    {
        root = newNode;
    }
    else
    {
        current = root;
        
        while (current != NULL)
        {
            trailCurrent = current;
            
            if (current->info == insertItem)
            {
                cout << "The item to be inserted is already "; 
                cout << "in the tree -- duplicates are not "
                    << "allowed." << endl; 
                return;
            }
            else if (current->info > insertItem)
            {
                current = current->lLink;
            }
            else
            {
                current = current->rLink;
            }
        }//end while
        
        if (trailCurrent->info > insertItem) 
        {
            trailCurrent->lLink = newNode;
        }
        else
        {
            trailCurrent->rLink = newNode;
        }
    }

} // end insert

template <class elemType>
void binaryTreeType<elemType>::deleteFromTree(nodeType<elemType>* &p)
{
    nodeType<elemType> *current; 
    //pointer to traverse the tree 
   
    nodeType<elemType> *trailCurrent; 
    //pointer behind current 
    
    nodeType<elemType> *temp; 
    //pointer to delete the node
    
    if (p == NULL)
    {
        cout << "Error: The node to be deleted is NULL."
        << endl;
    }
    else if (p->lLink == NULL && p->rLink == NULL)
    {
        temp = p;
        p = NULL; 
        delete temp;
    }
    else if (p->lLink == NULL)
    {
        temp = p;
        p = temp->rLink; 
        delete temp;
    }
    
    else if (p->rLink == NULL)
    {
        temp = p;
        p = temp->lLink; 
        delete temp;
    }
    else
    {
        current = p->lLink;
        trailCurrent = NULL;
        
        while (current->rLink != NULL)
        {
            trailCurrent = current;
            current = current->rLink;
        }//end while
        
        p->info = current->info;
        
        if (trailCurrent == NULL) //current did not move;
        {
             p->lLink = current->lLink;
        }
        else
        {
            trailCurrent->rLink = current->lLink;
        }
        
        delete current;
    }//end else
} //end delete from tree


template <class elemType>
void binaryTreeType<elemType>::deleteNode(const elemType& deleteItem)
{
    nodeType<elemType> *current; //pointer to traverse the tree 
    nodeType<elemType> *trailCurrent; //pointer behind current 
    bool found = false;
    
    if (root == NULL)
    {
        cout << "Cannot delete from an empty tree."
            << endl;
    }
    else
    {
        current = root;
        trailCurrent = root;
        while (current != NULL && !found)
        {
            if (current->info == deleteItem)
            {
                found = true;
            }
            else
            {
                trailCurrent = current;
                
                if (current->info > deleteItem)
                {
                    current = current->lLink;
                }
                else
                {
                    current = current->rLink;
                }
            }
        }//end while
        
        if (current == NULL)
        {
            cout << "The item to be deleted is not in the tree."
            << endl;
        }
        else if (found)
        {
            if (current == root)
            {
                deleteFromTree(root);
            }
            else if (trailCurrent->info > deleteItem)
            {
                deleteFromTree(trailCurrent->lLink);
            }
            else
            {
                deleteFromTree(trailCurrent->rLink);
            }
            
        }
        else
        {
            cout << "The item to be deleted is not in the tree." << endl;
        }
    }
} //end deleteNode



#endif /* BINARYTREE_H */

