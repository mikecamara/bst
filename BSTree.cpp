/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   BSTree.cpp
 * Author: mcmikecamara
 * 
 * Created on May 10, 2016, 3:59 PM
 */

#include "BSTree.h"

using namespace std;

// prototypes
template <class Item, class Key>
size_t tree_size(const BSTree<Item, Key>* root);

template <class Item, class Key>
void tree_clear(BSTree<Item, Key>*& root);

template <class Item, class Key>
size_t tree_depth(const BSTree< Item, Key>* root);

template <class Item, class Key>
size_t tree_size(const BSTree<Item, Key>* root)
{
    if (root == NULL)
    {
        return 0;
    }
    else 
    {
        return (tree_size(root->get_left()) + tree_size(root->get_right()) + 1);
    }
}


template <class Item, class Key>
void tree_clear(BSTree<Item, Key>*& root)
{
    if(root == NULL)
    {
        return;
    }
    else
    {
        tree_clear(root->get_left());
        tree_clear(root->get_right());
        delete root;
        root = NULL;
    }
}

template <class Item, class Key>
size_t tree_depth(const BSTree<Item, Key>* root)
{
    if (root == NULL || root->is_leaf())
    {
        return 0;
    }
    else if(tree_depth(root->get_left()) > tree_depth(root->get_right()))
    {
        return tree_depth(root->get_left()) + 1;
    }
    else 
    {
         return tree_depth(root->get_right()) + 1;
    }
}





