/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   main.cpp
 * Author: mcmikecamara
 *
 * Created on May 10, 2016, 12:02 PM
 */

#include <cstdlib>
#include <iostream>
#include "BST.h"
#include "BSTree.h"
#include "BinaryTree.h"
#include "stackType.h"

using namespace std;

/*
 * 
 */
int main() {

    int TreeKeys[10] = {51, 44, 23, 78, 98, 21, 56, 94, 12, 10};

    BST myTree;


    //BSTree myTreeTemplate;

    binaryTreeType<int> dateTree;

    dateTree.insert(11);
    dateTree.insert(7);
    dateTree.insert(12);
    dateTree.insert(8);
    dateTree.insert(44);

    // returns height I guess
    cout << dateTree.treeHeight() << "<-First print \n";

    // returns 0 if item not found or 1 if item found
    cout << dateTree.search(45) << "<-First print \n";
    
    dateTree.inorderTraversal();

    //cout<< dateTree.size(11);


    cout << "Printing the tree in order \n before adding numbers\n";

    myTree.printInOrder();

    for (int i = 0; i < 10; i++) {
        myTree.addLeaf(TreeKeys[i]);

        //myTreeTemplate.set_data(TreeKeys[i]);
    }
    cout << "Printing the tree in order \n after adding numbers\n";

    myTree.printInOrder();

    myTree.printChildren(myTree.returnRootKey());

    for (int i = 0; i < 10; i++) {
        myTree.printChildren(TreeKeys[i]);

        //cout << "this is important stuff->" << myTreeTemplate.get_data(TreeKeys[i]);
    }

    cout << "\n" << myTree.findSmallest() << "<- this is the smallest value " << "\n";

    myTree.removeNode(10);

    cout << "\n" << myTree.findSmallest() << "<- this is the smallest value " << "\n";


    // stack test

    stackType<int> stack(50);
    stackType<int> copyStack(50);
    stackType<int> dummyStack(100);
    stack.initializeStack();
    stack.push(23);
    stack.push(45);
    stack.push(38);
    copyStack = stack; //copy stack into copyStack
    cout << "The elements of copyStack: ";
    while (!copyStack.isEmptyStack()) {//print 

        cout << copyStack.top() << " ";
        copyStack.pop();
    }
    cout << endl;

    copyStack = stack;
    //testCopyConstructor(stack); //test the copy constructor

    if (!stack.isEmptyStack()) {
        cout << "The original stack is not empty." << endl
                << "The top element of the original stack: " << copyStack.top() << endl;
    }

    dummyStack = stack; //copy stack into dummyStack 
    cout << "The elements of dummyStack: ";

    while (!dummyStack.isEmptyStack()) //print dummyStack
    {
        cout << dummyStack.top() << " ";
        dummyStack.pop();
    }
    cout << endl;

    return 0;
}

//void testCopyConstructor(stackType<int> otherStack) {
//    if (!otherStack.isEmptyStack()) {
//        cout << "otherStack is not empty." << endl
//                << "The top element of otherStack: "
//                << otherStack.top() << endl;
//    }
//}

