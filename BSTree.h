/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   BSTree.h
 * Author: mcmikecamara
 *
 * Created on May 10, 2016, 3:59 PM
 */

#ifndef BSTREE_H
#define BSTREE_H

#include <utility>
#include <cstdlib>

template <class Item, class Key>
class BSTree {
public:
    BSTree(const Key& k, const Item& data, BSTree<Item, Key>* l, BSTree<Item, Key>* r);
    BSTree(const BSTree<Item, Key>*& copy);
    
    
    // Mutable member functions
    Key& get_key(){return key;}
    Item& get_data(){return entry;}
    BSTree<Item, Key>*& get_left(){return left;}
    BSTree<Item, Key>*& get_right(){return right;}
    void set_key(const Key& k){key = k;}
    void set_data(const Item& data){entry = data;}
    void set_left(BSTree* l){left = l;}
    void set_right(BSTree* r){right = r;}
    
    // Immutable member functions
    const Key& get_key() const{return key;}
    const Item& get_data() const{return entry;}
    const BSTree<Item, Key>* get_left() const {return left;}
    const BSTree<Item, Key>* get_right() const {return right;}
    
    // Convenience function
    bool is_leaf() const {return (left == NULL) && (right == NULL);}
    
private:
    Key key;
    Item entry;
    
    BSTree<Item,Key>* right;
    BSTree<Item, Key>* left;

};

//template <class Item, class Key>
//BSTree<Item, Key>::BSTree(const Key& k, const Item& data = Item(), BSTree<Item, Key>* l = NULL, BSTree<Item, Key>* r = NULL)
//{
//    key = k;
//    entry = data;
//    left = l;
//    right = r;
//}
//
//template <class Item, class Key>
//BSTree<Item, Key>::BSTree(const BSTree<Item, Key>*& copy)
//{
//    if(copy == NULL)
//    {
//        this == NULL;
//    }
//    else
//    {
//        entry = copy.entry;
//        key = copy.key;
//        left = copy.left;
//        right = copy.right;
//    }
//}

#endif /* BSTREE_H */

